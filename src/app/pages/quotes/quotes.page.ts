import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import html2canvas from 'html2canvas';
import domtoimage from 'dom-to-image';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.page.html',
  styleUrls: ['./quotes.page.scss'],
})
export class QuotesPage implements OnInit {

  data: any = [];
  loading = true;

  gambar: any;

  constructor(
    public http: HttpClient,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(event) {

    if (!event) { event = '' }

    let url = 'https://ubmart.online/api_iq/quotes_read.php?text_en=' + event + '&text_in=' + event;

    this.http.get(url).subscribe((data) => {
      console.info(data);

      this.data = data;

      this.loading = false;
    }, error => {
      console.info(error.error);

      this.loading = false;
    });
  }

  save() {
    domtoimage.toJpeg(document.getElementById('my-node'), { quality: 0.95 })
    .then(function (dataUrl) {
        var link = document.createElement('a');
        link.download = 'my-image-name.jpeg';
        link.href = dataUrl;
        link.click();
    });
  }

}
